// TODO: modify the struct so it holds both the plaintext
// word and the hash.

typedef struct node {
	char* ppw;
	char* hpw;
	struct node *left;
	struct node *right;

} node;

void insert(char* key, char* hkey, node **leaf);

void print(node *leaf);

node *search(char* key, node *leaf);
