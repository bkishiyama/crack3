#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "bintree.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// TODO
// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{
    FILE *f = fopen(filename, "r");

    int size =PASS_LEN;
    char **pwds=(char**)malloc(size*sizeof(char*));
    char str[size];
    int i=0;
    while (fgets(str, PASS_LEN, f) != NULL)
    {
        str[strlen(str) - 1] = '\0'; //trim off \n
        //increase size of the pwds array if neccessary
        if (i == size)
        {
            size = size + 100;
            char **newarr = (char **)realloc(pwds, size * sizeof(char *));
            if (newarr != NULL) pwds = newarr;
            else exit(1);
        }
        char *newstr = (char *)malloc((strlen(str) + 1) * sizeof(char));
        strcpy(newstr, str);
        pwds[i] = newstr;
        i++;
    }
    //increase the size for the nulled ending if necessary
    if (i == size)
    {
        size = size + 1;
        char **newarr = (char **)realloc(pwds, size * sizeof(char *));
        if (newarr != NULL) pwds = newarr;
        else exit(1);
    }
    //set last element=NULL so that the readthrough of this array, done in main is possible
    pwds[i] = NULL;
    //close file
    fclose(f);
    return pwds;
}


// TODO
// Read in the dictionary file and return the tree.
// Each node should contain both the hash and the
// plaintext word.
node *read_dict(char *filename)
{

    FILE *f = fopen(filename, "r");
    char str[50];
    node *btree=NULL;


    while(fgets(str, 50, f) !=NULL)
    {
        str[strlen(str) - 1] = '\0'; //trim
        char *pstr = (char *)malloc((strlen(str) + 1) * sizeof(char));//area to store plain text password
        strcpy(pstr, str);//increase length of string
        char *hash  = (char *)malloc(HASH_LEN * sizeof(char)); //allocate memory for a hash
        hash = md5(pstr, strlen(pstr)); //md5 passwords
        insert(pstr, hash, &btree);//place the plain text password and hash into the binary tree

    }
    return btree;
}


int main(int argc, char *argv[])
{

    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into a binary tree
    node *dict = read_dict(argv[2]);

    // TODO
    // For each hash, search for it in the binary tree.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int count = 0;
    while (hashes[count] != NULL)
    {
        node *pass = search(hashes[count], dict);
        if(pass != NULL)
            printf("%s  %s\n", hashes[count], pass->ppw);
        count++;
    }

}
