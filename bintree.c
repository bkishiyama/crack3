#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bintree.h"

void insert(char* key, char* hkey, node **leaf)
{
    if (*leaf == NULL)
    {
        *leaf = (node *) malloc(sizeof(node));
        (*leaf)->ppw = (char *)malloc(50 * sizeof(char)); // Allocate memory for the plaintext password
        (*leaf)->hpw = (char *)malloc(33 * sizeof(char)); // Allocate memory for the hashed password
        strcpy((*leaf)->ppw,key);
        strcpy((*leaf)->hpw,hkey);
        (*leaf)->left = NULL;    
        (*leaf)->right = NULL;  
    }
    else if (strcmp(hkey, (*leaf)->hpw) < 0)
        insert (key, hkey, &((*leaf)->left) );
    else if (strcmp(hkey, (*leaf)->hpw) > 0)
        insert (key, hkey, &((*leaf)->right) );
}

void print(node *leaf)
{
	if (leaf == NULL) return;
	if (leaf->left != NULL) print(leaf->left);
	printf("%s\n", leaf->hpw);
	printf("%s\n", leaf->ppw);
	if (leaf->right != NULL) print(leaf->right);
}

// TODO: Modify so the key is the hash to search for
node *search(char* key, node *leaf)
{
 /* if (leaf != NULL)
  {
      if (strcmp(key, leaf->hpw) == 0)
      {
         return leaf;
      }
      else if (strcmp(key, leaf->hpw) < 0)
         return search(key, leaf->left);
      else
         return search(key, leaf->right);
  }
  else return NULL;*/
  if (leaf != NULL)
  {
      if ( strcmp( key, leaf->hpw ) == 0)
         return leaf;
      else if (strcmp(key, leaf->hpw) < 0)
         return search(key, leaf->left);
      else
         return search(key, leaf->right);
  }
  else return NULL;
}


